#!/bin/bash

if [ -z $1 ]; then
	echo "Please provide a version number : exemple, provide 9.0.0 as an argument to the script"
	exit 1
fi

if [[ $1 =~ [0-9]+\.[0-9]+\.[0-9] ]];then
	echo "Correct version number $1"
	echo "Deploying the cookbook ..."
	# cookbook is on two digit, there is no notable modification of doc for patch version
	COOKBOOK_VERSION=$(echo "$1" | cut -d'.' -f-2)
	# remove previous doc of same version
	rm -rf /var/www/doc/CookBook-$COOKBOOK_VERSION || true
	sudo tar -xzf /var/www/archives/packages/ci/staging/CookBook-$1-html.tar.gz -C /var/www/doc
	mv /var/www/doc/CookBook-$1 /var/www/doc/CookBook-$COOKBOOK_VERSION || true
	cd /var/www/wordpress-4.4
	sudo ln -nsf /var/www/doc/CookBook-$COOKBOOK_VERSION CookBook-$COOKBOOK_VERSION
	sudo ln -nsf CookBook-$COOKBOOK_VERSION CookBook


	echo "Deploying the doxygen documentation ..."
	cd /var/www/doc
	sudo tar -xjf /var/www/archives/packages/ci/staging/OTB-Doxygen-$1.tar.bz2 -C .
	sudo mv html doxygen-$1
	cd /var/www/wordpress-4.4
	sudo ln -nsf ../doc/doxygen-$1 doxygen-$1
	sudo ln -nsf doxygen-$1 doxygen
else
	echo "Invalid version number $1, expected three digits separated by '.' (e.g. 9.0.0)"
fi
