#!/bin/bash

if [ ! $1 ]; then
    $1 = "9.0.0"
fi

cd /var/www/archives/packages

echo "** copying ci archives to the archives/OTB folder **"
sudo cp ci/staging/OTB-$1-Linux_RedHat.tar.gz archives/OTB/OTB-$1-Linux_RedHat.tar.gz
sudo cp ci/staging/OTB-$1-Linux.tar.gz archives/OTB/OTB-$1-Linux.tar.gz
sudo cp ci/staging/OTB-$1-Linux_RedHat-Dependencies.tar.gz archives/OTB/OTB-$1-Linux_RedHat-Dependencies.tar.gz
sudo cp ci/staging/OTB-$1-Linux-Dependencies.tar.gz archives/OTB/OTB-$1-Linux-Dependencies.tar.gz
sudo cp ci/staging/OTB-$1-Linux-Core.tar.gz archives/OTB/OTB-$1-Linux-Core.tar.gz
sudo cp ci/staging/OTB-$1-Linux-FeaturesExtraction.tar.gz archives/OTB/OTB-$1-Linux-FeaturesExtraction.tar.gz
sudo cp ci/staging/OTB-$1-Linux-Hyperspectral.tar.gz archives/OTB/OTB-$1-Linux-Hyperspectral.tar.gz
sudo cp ci/staging/OTB-$1-Linux-Learning.tar.gz archives/OTB/OTB-$1-Linux-Learning.tar.gz
sudo cp ci/staging/OTB-$1-Linux-Miscellaneous.tar.gz archives/OTB/OTB-$1-Linux-Miscellaneous.tar.gz
sudo cp ci/staging/OTB-$1-Linux-Sar.tar.gz archives/OTB/OTB-$1-Linux-Sar.tar.gz
sudo cp ci/staging/OTB-$1-Linux-Segmentation.tar.gz archives/OTB/OTB-$1-Linux-Segmentation.tar.gz
sudo cp ci/staging/OTB-$1-Linux-StereoProcessing.tar.gz archives/OTB/OTB-$1-Linux-StereoProcessing.tar.gz
sudo cp ci/staging/OTB-$1-win64.zip archives/OTB/OTB-$1-win64.zip

echo "** create symbolik links **"
sudo ln -s archives/OTB/OTB-$1-Linux.tar.gz OTB-$1-Linux.tar.gz
sudo ln -s archives/OTB/OTB-$1-Linux_RedHat.tar.gz OTB-$1-Linux_RedHat.tar.gz
sudo ln -s archives/OTB/OTB-$1-win64.zip OTB-$1-Win64.zip
sudo ln -s archives/OTB/OTB-$1-Linux-Core.tar.gz OTB-$1-Linux-Core.tar.gz
sudo ln -s archives/OTB/OTB-$1-Linux-FeaturesExtraction.tar.gz OTB-$1-Linux-FeaturesExtraction.tar.gz
sudo ln -s archives/OTB/OTB-$1-Linux-Hyperspectral.tar.gz OTB-$1-Linux-Hyperspectral.tar.gz
sudo ln -s archives/OTB/OTB-$1-Linux-Learning.tar.gz OTB-$1-Linux-Learning.tar.gz
sudo ln -s archives/OTB/OTB-$1-Linux-Sar.tar.gz OTB-$1-Linux-Sar.tar.gz
sudo ln -s archives/OTB/OTB-$1-Linux-Segmentation.tar.gz OTB-$1-Linux-Segmentation.tar.gz
sudo ln -s archives/OTB/OTB-$1-Linux-StereoProcessing.tar.gz OTB-$1-Linux-StereoProcessing.tar.gz
sudo ln -s archives/OTB/OTB-$1-Linux-Dependencies.tar.gz OTB-$1-Linux-Dependencies.tar.gz
