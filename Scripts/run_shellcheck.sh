#!/bin/sh

if [ $# -lt 1 ]; then
    echo "$0: required one argument which is path to OTB source directory" 
fi

OTB_SOURCE_DIR="$1"

SHELLCHECK=$(which shellcheck)
if [ -z "$SHELLCHECK" ]; then
    echo "$0: Cannot find shellcheck. Make sure it is installed and can be found in PATH"
fi

"$SHELLCHECK" "$OTB_SOURCE_DIR/CMake/otbcli.sh.in"
"$SHELLCHECK" "$OTB_SOURCE_DIR/CI/SuperbuildDownloadList.sh"
"$SHELLCHECK" "$OTB_SOURCE_DIR/CI/deploy.sh"
"$SHELLCHECK" "$OTB_SOURCE_DIR/CI/deploy-prod.sh"
"$SHELLCHECK" "$OTB_SOURCE_DIR/CI/conda_build.sh"
"$SHELLCHECK" "$OTB_SOURCE_DIR/CI/deploy-archive.sh"
"$SHELLCHECK" "$OTB_SOURCE_DIR/CI/contributors_check.sh"
"$SHELLCHECK" "$OTB_SOURCE_DIR/CI/otb_coverage.sh"
"$SHELLCHECK" "$OTB_SOURCE_DIR/Packaging/Files/post_install.sh"
"$SHELLCHECK" "$OTB_SOURCE_DIR/Packaging/Files/check_python_env.sh"
"$SHELLCHECK" "$OTB_SOURCE_DIR/Packaging/Files/uninstall_otb.sh"
"$SHELLCHECK" "$OTB_SOURCE_DIR/Packaging/Files/recompile_bindings.sh"
#"$SHELLCHECK" "$OTB_SOURCE_DIR/CMake/otbcli_app.sh.in"

#"$SHELLCHECK" "$OTB_SOURCE_DIR/CMake/otbgui.sh.in"
#"$SHELLCHECK" "$OTB_SOURCE_DIR/CMake/otbgui_app.sh.in"

#"$SHELLCHECK" "$OTB_SOURCE_DIR/Packaging/Files/linux_pkgsetup.in"
#"$SHELLCHECK" "$OTB_SOURCE_DIR/Packaging/Files/macx_pkgsetup.in"
